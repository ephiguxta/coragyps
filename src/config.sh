#!/usr/bin/env bash

config_path() {
   : ''' [func scope]
   configure the path/dirs to use the tool
   '''

   local bin_path="${HOME}/.local/bin/coragyps"
   # var used to store user answer.
   local answer=''

   test -d "$bin_path" && {
      echo "dir exists, do you want to replace them?"

      # the user should answer 'y' or 'n' only.
      while [[ ! "$answer" =~ [yYnN] ]]; do
         read -p '[yY / nN] ' answer
      done

      test "$answer" == 'y' && {
         # at the moment bin path is configured,
         # now we should insert the tool at the dir
         rm -rf "$bin_path"
      }
   }

   mkdir -p "$bin_path"
   insert_bin
}

insert_bin() {
   : ''' [func scope]
   copy all necessary data to use the tool
   '''

   local bin_path="${HOME}/.local/bin/coragyps"
   local bashrc="${HOME}/.bashrc"
   local new_path_cmd="export PATH=\"\${PATH}:${bin_path}\""

   #TODO:  test if file exists
   cp coragyps.sh "${bin_path}/coragyps"

   # enabling the tool on shell

   #TODO: check if tool path is on the bashrc file
   echo "$new_path" >> "$bashrc"
   source "$bashrc"

   exit 0
}

main() {
   config_path
}

main "$@"
